import java.applet.Applet;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PingPong extends Applet implements Runnable, KeyListener {

    private int option = 1, newLineDistance = 30, firstLineY = 300;
    private final int WIDTH = 700, HEIGHT = 500;
    private boolean p2Exists = false;
    private Thread thread;
    private PlayerPaddle p1, p2;
    private Ball ball;
    private ComputerPaddle cp;

    private Graphics graphics;
    private Image image;
    private String[] gameModeOptions = {"1 - tryb jednoosobowy", "2 - tryb wielosobowy"};


    @Override
    public void init() {
        this.resize(WIDTH, HEIGHT);
        this.addKeyListener(this);
        p1 = new PlayerPaddle(1);
        ball = new Ball();
        image = createImage(WIDTH, HEIGHT);
        graphics = image.getGraphics();
        thread = new Thread(this);
    }

    @Override
    public void paint(Graphics g) {
        graphics.setColor(Color.GRAY);
        graphics.fillRect(0, 0, WIDTH, HEIGHT);

        if (thread.getState() == Thread.State.NEW) {
            graphics.setColor(Color.GREEN);
            graphics.setFont(new Font("TimesRoman", Font.PLAIN, 50));
            graphics.drawString("NACIŚNIJ SPACJĘ", 135, 275);
            graphics.setFont(new Font("TimesRoman", Font.PLAIN, 25));

            for (String line : gameModeOptions) {
                graphics.drawString(line, 230, firstLineY);
                firstLineY += newLineDistance;

            }

        } else if (ball.getX() < -10 || ball.getX() > 710) {
            graphics.setColor(Color.GREEN);
            graphics.setFont(new Font("TimesRoman", Font.PLAIN, 50));
            graphics.drawString("KONIEC GRY", 200, 275);
        } else {
            p1.draw(graphics);
            ball.draw(graphics);
            if (!p2Exists) cp.draw(graphics);
            else p2.draw(graphics);
        }
        g.drawImage(image, 0, 0, this);
    }

    @Override
    public void update(Graphics g) {
        paint(g);
    }

    @Override
    public void run() {
        boolean isRunning = true;
        if (option == 1) {
            cp = new ComputerPaddle(2, ball);
            p2Exists = false;
        } else if (option == 2) {
            p2 = new PlayerPaddle(2);
            p2Exists = true;
        }

        while (isRunning) {
            System.out.println(option);
            System.out.println(thread.getState());
            p1.move();
            ball.move();

            if (!p2Exists) {
                cp.move();
                ball.checkCollision(p1, cp);
            } else if (p2Exists) {
                p2.move();
                ball.checkCollision(p1, p2);
            }

            repaint();
            if (ball.getX() < -10 || ball.getX() > 710) {
                isRunning = false;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()) {
            case KeyEvent.VK_1:
                option = 1;
                break;
            case KeyEvent.VK_2:
                option = 2;
                break;
            case KeyEvent.VK_W:
                p1.setUpAcceleration(true);
                break;
            case KeyEvent.VK_S:
                p1.setDownAcceleration(true);
                break;
            case KeyEvent.VK_UP:
                if (option == 2) {
                    p2.setUpAcceleration(true);
                }
                break;
            case KeyEvent.VK_DOWN:
                if (option == 2) {
                    p2.setDownAcceleration(true);
                }
                break;
            case KeyEvent.VK_SPACE:
                if (!thread.isAlive()) {
                    try {
                        thread.start();
                    } catch (IllegalThreadStateException exception) {
                        exception.printStackTrace();
                    }
                }
                break;
            case KeyEvent.VK_ESCAPE:
                if (!thread.isAlive()) {
                    System.exit(0);
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                p1.setUpAcceleration(false);
                break;
            case KeyEvent.VK_S:
                p1.setDownAcceleration(false);
                break;
            case KeyEvent.VK_UP:
                if (option == 2) {
                    p2.setUpAcceleration(false);
                }
                break;
            case KeyEvent.VK_DOWN:
                if (option == 2) {
                    p2.setDownAcceleration(false);
                }
                break;
        }
    }
}
