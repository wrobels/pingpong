import java.awt.*;

public class PlayerPaddle implements Paddle {

    private double y, ySpeed;
    private boolean upAcceleration, downAcceleration;
    private int x;
    final private double GRAVITY = 0.94;

    PlayerPaddle(int player) {
        upAcceleration = false;
        downAcceleration = false;
        y = 210;
        ySpeed = 0;
        x = player == 1 ? 20 : 660;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(Color.white);
        g.fillRect(x, (int) y, 20, 80);
    }

    void setUpAcceleration(boolean inputValue) {
        upAcceleration = inputValue;
    }

    void setDownAcceleration(boolean inputValue) {
        downAcceleration = inputValue;
    }

    @Override
    public void move() {
        if (upAcceleration) {
            ySpeed -= 2;
        } else if (downAcceleration) {
            ySpeed += 2;
        } else {
            ySpeed *= GRAVITY;
        }

        if (ySpeed >= 5)
            ySpeed = 5;
        else if (ySpeed <= -5)
            ySpeed = -5;
        y += ySpeed;

        if (y < 0)
            y = 0;
        else if (y > 420)
            y = 420;
    }

    @Override
    public int getY() {
        return (int) y;
    }
}
