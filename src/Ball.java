import java.awt.*;
import java.util.Random;

class Ball {

    private double x, y, xSpeed, ySpeed;
    private int sign;

    Ball() {
        sign = getRandomSign();
        this.x = 350;
        this.y = 250;
        xSpeed = 2 * sign;
        ySpeed = sign;
    }

    void draw(Graphics g) {
        g.setColor(Color.red);
        g.fillOval((int) x - 10, (int) y - 10, 20, 20);
    }

    void move() {
        x += xSpeed;
        y += ySpeed;

        if (y < 10 || y > 490) {
            ySpeed = -ySpeed;
        }
    }

    int getX() {
        return (int) x;
    }

    int getY() {
        return (int) y;
    }

    void checkCollision(Paddle p1, Paddle p2) {
        if (x <= 50) {
            if (y >= p1.getY() && y <= p1.getY() + 80) {
                xSpeed = -xSpeed;
            }
        } else if (x >= 650) {
            if (y >= p2.getY() && y <= p2.getY() + 80) {
                xSpeed = -xSpeed;
            }
        }
    }

    void checkCollision(Paddle p1, ComputerPaddle p2) {
        if (x <= 50) {
            if (y >= p1.getY() && y <= p1.getY() + 80) {
                xSpeed = -xSpeed;
            }
        } else if (x >= 650) {
            if (y >= p2.getY() && y <= p2.getY() + 80) {
                xSpeed = -xSpeed;
            }
        }
    }

    private int getRandomSign() {
        Random rand = new Random();
        if (rand.nextBoolean())
            return -1;
        else
            return 1;
    }
}
