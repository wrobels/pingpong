import java.awt.*;
import java.util.Random;

public class ComputerPaddle implements Paddle {

    private double y;
    private int x;
    private Ball ball;
    int sign;


    ComputerPaddle(int player, Ball b) {
        ball = b;
        if (player == 1) {
            x = 20;
        } else if (player == 2) {
            x = 660;
        }
        y = 210;
    }


    @Override
    public void draw(Graphics g) {
        g.setColor(Color.white);
        g.fillRect(x, (int) y, 20, 80);
    }


    @Override
    public void move() {
        // sign = getRandomSign();

        y = ball.getY() - 40;

        // y = new Random().nextInt(420);

        if (y < ball.getY()) {
            y += 2;
        } else if (y > ball.getY()) {
            y -= 2;
        }
        if (y < 0)
            y = 0;
        else if (y > 420)
            y = 420;
    }

    @Override
    public int getY() {
        return (int) y;
    }

    public int getRandomSign() {
        Random rand = new Random();
        if (rand.nextBoolean())
            return -1;
        else
            return 1;
    }
}
